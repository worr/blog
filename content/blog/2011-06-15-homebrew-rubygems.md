+++
title = "Ruby and Gems from Homebrew on Mac OSX"
+++

Hey, so I bought a Macbook Air a month ago, and I've grown to absolutely adore
it. It basically comes down to that I have an OS that I don't need to fix
constantly, while also having access to all of the utilities I'm used to using
on my Linux/UNIX environments.

I use [Homebrew](http://mxcl.github.com/homebrew/) to manage my software,
because it seems to be the best package management solution for OS X. I
haven't really tried any others, but I have no inclination to after using
Homebrew for the past few months.

Oh and I also learned Ruby. And I kind of like it so far.

Regardless, this post isn't about my Ruby experience thus far. It's just
documenting a minor annoyance. When I install gems using ruby1.9 from
Homebrew, the gems don't make a symlink in /usr/local, and the internet tells
me the right thing to do is to make it myself.

Well fuck that.

Here's a brief ruby program that will insert these links for you! I'm not
claiming to be a Ruby expert, or even competent, so run this at your own risk.
It can get a little destructive, and makes some assumptions.

```perl
#!/usr/bin/env ruby

require 'RubyGems'

installed_gems = []
prefix = '/usr/local/bin/'
ruby = '/usr/local/Cellar/ruby'

Gem::Specification::all_names().reverse.each do |gem|
  (name, version) = ['', '']
  gem.match(/([\w\-]+)\-([\w\.]+)/) do |m|
    name = m.captures[0]
    version = m.captures[1]
  end

  Gem::Specification.new do |s|
    s.name = name
    s.version = version

    installed_gems.push(s)

  end

  installed_gems.delete_if do |i|
    name == i.name and Gem::Version.new(version) > Gem::Version.new(i.version)
  end
end


installed_gems.each do |gem|
  if File.directory? gem.bin_dir  then
    Dir.entries(gem.bin_dir).each do |file|
     fullpath = gem.bin_dir + '/' + file
     newpath = prefix + file

     if not File.directory? fullpath  then
       if File.symlink? newpath then
         if File.readlink(newpath).match %r{/usr/local/Cellar/ruby} then
           puts "Linking #{fullpath} to #{newpath}..."
           File.unlink newpath
           File.symlink fullpath, newpath
         end
       end
     end
    end
  end
end
```

Hope that helps.
