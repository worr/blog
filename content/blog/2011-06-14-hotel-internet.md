+++
title = "Hotel Internet"
+++

Have you ever been to a hotel, or an airport, that served wireless? And the
after you connected, you get redirected to a page where you pay money, or
make an account? Those are called captive portals. Setting up your own is
really easy!

The old de facto FOSS software for captive portals was
[Chillispot](www.chillispot.info). However, this software is old, and broken,
and doesn't really work anymore. So, I used its drop in replacement,
[CoovaChilli](coova.org/CoovaChilli). CoovaChilli is totally compatible with
Chillispot, so if you've used Chillispot before, you can use your old
configuration with CoovaChilli without any modification.

CoovaChilli handles serving DHCP to your clients, as well as setting up
routing for you. Your server acts as a router for traffic for auth'd clients.
CoovaChilli creates a tun device that it tunnels traffic through, kind of like
a VPN. 
Since it serves DHCP, you can configure it to give your clients DNS servers,
proxy information, etc. CoovaChilli will also load up a whole bunch of
firewall rules that will restrict what resources clients can access on your
network.

First step, set up a simple FreeRadius configuration. I left mine pretty with the
Debian defaults, and just created some accounts for testing. This system isn't
in production, so I don't care about tuning my radius setup. And FreeRadius
isn't the point of this entry.

Once, you have FreeRadius going, download, compile and install CoovaChilli.
CoovaChilli doesn't have Debian or Ubuntu packages that work very well...and
building packages from the source doesn't work perfectly either, so just
./configure, make and make install. Don't worry, there's a make deinstall in
case you want to remove it cleanly.

After building it, copy /etc/chilli/defaults to /etc/chilli/config and start
editing. Some things you'll want to change:

  - HS\_DNS1 and HS\_DNS2: the DNS servers given to your hosts
  - HS\_RADSECRET: the secret required to talk to your Radius server
  - HS\_UAMSECRET: the secret required to talk to your hotspot
  - HS\_UAMSERVER: URL to the login page
  - HS\_POSTAUTH\_PROXY: we'll talk about this later

You'll also probably want to set your interfaces. Your WAN interface needs to
be set through your OS, but your LAN interface requires absolutely no
configuration through your OS - CoovaChilli will do it when it comes up.

It's also important to run this:
    sysctl net.ipv4.ip_forward=yes

That will enable IP forwarding on your system.

The other values can be left with their default values. We're not going to set
up a custom login page because we really don't care. The default is fine for
us. You're going to need to install [haserl](haserl.sourceforge.net) to use
it.

Well cool, we now have a captive portal! Run the init script, and bam. You now
have a captive portal.

Tomorrow, we'll talk about going beyond basic configuration, and content
modification using Squid3 and GreasySpoon.
