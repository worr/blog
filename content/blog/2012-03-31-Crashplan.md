+++
title = "CrashPlan on FreeBSD 9.0, A HOWTO"
+++

This was an incredibly productive week for me. I got a lot done, including
setting up Nagios for my servers, starting a CSH Photohunt Android app, working
a bit on a game that I've been writing, and most importantly, rolling out
network backups.

And that's what this post is about.

[CrashPlan](http://www.crashplan.com/) is a cloud backup service that allows
users to backup *unlimited data* for a small monthly fee. For people with large
amounts of data, this is a significantly cheaper solution than Amazon S3 or
Carbonite or similar. I quickly purchased the unlimited backup service, and
decided to start backing up all of my computers using their service.

While I really hate the GUI tool, I think the service itself is fantastic.
[Here's a fantastic review](http://www.onlinebackupsreview.com/crashplan.php) of
the backup service, as well as a coupon.

Let me first start out by saying that I already do backups. I do Time Machine
and rsync backups to my FreeBSD NAS. What I was looking for initially was a
cloud backup service that allowed me to backup my entire NAS to the internet (if
I say "cloud" one more time...). CrashPlan doesn't "support" FreeBSD. That is to
say, they don't officially provide a client to allow you to backup from
FreeBSD...but it's more than possible.

So without further ado...

## How to backup your FreeBSD 9.0 machine with CrashPlan

First, set up and install the linux compatibility environment. Open up your
make.conf and add these two lines:

    OVERRIDE_LINUX_BASE_PORT=f10
    OVERRIDE_LINUX_NONBASE_PORTS=f10

We want to install the Fedora 10-based environment, since it's the newest and
best supported one at the time of this writing. Then, load the linux.ko kernel
module, and add the option startup option to your rc.conf.

    # kldload linux.ko
    # echo "linux_enable=\"YES\"" >> /etc/rc.conf
    # echo "crashplan_enable=\"YES\"" >> /etc/rc.conf

Now, install the compat environment, java and linproc

    # portmaster emulators/linux_base-f10
    # portmaster sysutils/linux-procps
    # portmaster java/linux-sun-jre16

Add linproc to your /etc/fstab, as well as some other mounts while we're here

    # echo "linproc /compat/linux/proc linprocfs rw 0 0" >> /etc/fstab
    # echo "/usr /backup/usr nullfs rw 0 0" >> /etc/fstab
    # echo "/var /backup/var nullfs rw 0 0" >> /etc/fstab

When you're in the linux compat environment, /usr and /var are resolved to
/compat/linux/usr and /compat/linux/var transparently. We need those nullfs
mounts so that we can actually back up /usr and /var. Those are a bit important.

Now mount the shit out of everything.

Download crashplan somewhere under /compat/linux, and then chroot into your
linux environment. Run the install script, and back out.

    # chroot /compat/linux/ /bin/bash
    # cd CrashPlan-install && ./install.sh

chrooting is not absolutely necessary, but whatever. Moving on! We have some
stuff to configure with CrashPlan before it'll run well, and we're going to need
to find/write ourselves an init script as well!

First, in /compat/linux/usr/local/crashplan/bin/run.conf, modify the two lines
to end with this:

    SRV_JAVA_OPTS="... -Djava.nio.channels.spi.SelectorProvider=sun.nio.ch.PollSelectorProvider"
    GUI_JAVA_OPTS="... -Djava.nio.channels.spi.SelectorProvider=sun.nio.ch.PollSelectorProvider"

The important part is the last define on both lines. CrashPlan uses epoll, which
isn't available to us on FreeBSD. That line essentially tells CrashPlan to
behave itself on our platform.

Another thing we ought to do is to tell CrashPlan where Java is. Add a line to
/compat/linux/usr/local/crashplan/install.vars that does that

    JAVACOMMON=<YOUR PATH TO LINUX JAVA>

Neat! Now we just need an init script...and we can get that from [one of the two
sources I got some of this information
from!](http://blog.oddbit.com/2011/05/installing-crashplan-under-freebsd-8.html)
All we care about is the init script at the bottom. We've already done all of
the other steps listed (and more!), so don't worry about those. We've done some
things differently, but IMO this is a better setup.

Copy the contents of that initscript to /usr/local/etc/rc.d/crashplan, and
you're good to go. You can start it with
    
    # service crashplan start

[Here](http://kim.scarborough.chicago.il.us/do/nerd/tips/crashplan) is the other
source of my information regarding CrashPlan installation on FreeBSD.

[Here](http://stgsupport.crashplan.com/doku.php/how_to/configure_a_headless_client)
is the official documentation for running CrashPlan headless.

Happy backups!
