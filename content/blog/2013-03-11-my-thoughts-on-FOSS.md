+++
title = "My thoughts on Open Source"
+++

Surprisingly enough, this post *is not* for my FOSS class.

Anyway, [this](http://www.wired.com/opinion/2013/03/github/) article has been
circulating around my network lately, and it led me to think about my stance on
open source.

I love open source software, both writing it and using it.

Why?

First and foremost, I'm a student. Learning is quite literally my profession.
And hopefully after I graduate in May, I'll continue to learn. I love technology
because to be truly successful, you need to continue to learn throughout your
career. 

Open source allows me to learn by working with other people's code. Whenever I
jump into a large codebase, I tend to pick up techniques and design strategies
employed by the original programmer. By doing this, I've also gotten
progressively better at jumping into large projects quickly and figuring out
exactly what I need to do to add whatever feature or fix whatever bug.

I also tend to get frustrated by poor software. There are a lot of open source
projects that I like that lack features, or are bug-ridden. Whenever I get
pissed off by something that doesn't work, I can fix it. And then I can push my
changes upstream and fix it for everyone. Not only is it empowering, but I don't
have to change my workflow/program to account for other people's broken code.

A benefit of pushing your code upstream is the feedback you get on it. Most open
source developers will analyze your code before they merge it in, give you
feedback and allow you to fix your change. It's a great way to get some
constructive criticism on your code.

But the best benefit to working on open source is how it enhances my résumé. As
I mentioned before, I'm a student. There was a point where I was struggling to
get my first post-college job in the field. Having a github filled with open
source projects that I've pushed to as well as open source projects of my own
was a huge advantage over the competition - it helped me really stand out.

Never will I parrot the fucking notion that all software should be Free - it
shouldn't be. I'll never support viral open source licensing - that's not
freedom and that hurts businesses, both small and large.

But I will say that open source is invaluable to students looking to get out
into industry, and that it allows people already in industry to train old
skills as well as learn new ones.
