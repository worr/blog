+++
title = "Fixing Sugar bugs"
+++

After a late start, Matt and I have been blazing ahead on development of
Sugar. As of this writing, we have 6 (almost 7) patches merged into Sugar
upstream.

The biggest hurdle in getting Sugar installed was that I wasn't running it on
a supported OS. I was building and testing on Fedora 19, where it only
supports Fedora 18 and Ubuntu whateverthefuck.

As a result, my first 5 patches fixed various problems with the build system,
including adding support for Fedora 19. After some vigorous testing and
discussion, all 5 patches were merged in.

Afterwards, I found a crash in the journal caused by mousing over the Pippy
activity icon. The crash was caused by a call to a deprecated function...a
function prevalent across the entirety of Sugar. My 6th and 7th patches are
fixing this sweeping problem.

While we haven't quite fixed the kinds of usability bugs we first set out to
fix, we are making a significant impact on Sugar and are helping fix the
contribution process.
