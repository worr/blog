+++
title = "Introduction to Perl"
+++

Hey everyone! I made a Perl seminar for [CSH](http://www.csh.rit.edu/)!

[Here it is!](https://docs.google.com/presentation/d/1XXIpO0MYS70NHcdBvvLoXwsBMM_4IPnas9Vmd6h892s/edit?usp=sharing)

[And here is the code!](https://docs.google.com/folder/d/0B2CJPQY-C9ItQVE4TVNEckc4N3c/edit?usp=sharing)

This isn't even close to conclusive, but it does give you a basic introduction.

I cover certain topics that are important, and omit ones that are actually bad (default variable, 2 argument open).
