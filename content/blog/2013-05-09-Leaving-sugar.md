+++
title = "Leaving sugar behind...or am I?"
+++

Well, my H/FOSS class is coming to an end. In a few short hours, I'll finish 
my last H/FOSS class with a presentation on our work on Sugar (coincidentally
it's also my last college class).

At this point, I've realized that Sugar is a fairly disorganized project. It
seems like it's constantly in flux, with multiple wikis, bugtrackers, git repos,
etc. It's very confusing to get started deving on - probably the most
difficult project to enter in my experience in FOSS.

That being said, walking away will be hard...well, not that hard since I don't
plan on walking away just yet.

I was assigned some tasks by the team after my initial set of patches, so I'll
make sure to finish those up. Then what? I'll probably run away and never look
back.

While I can appreciate the work the Sugar devs do, it's not a project that I
find particularly compelling, so I plan on leaving it once I've submitted my
last set of patches. 
