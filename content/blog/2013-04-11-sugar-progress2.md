+++
title = "Week 2 in Sugarland"
+++
Hey all,

It's Thursday during H/FOSS class, which means that it's time for an update on
my progress with Sugar.

We've made very little progress - we've uncovered some more network-related
usability bugs, but haven't produced any working code yet.

Our environments have been set up, however Matt's got blown away in an OS
reinstall.
