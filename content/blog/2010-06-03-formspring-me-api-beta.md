+++
title = "Formspring.me API beta"
+++
formspring.me released a beta version of their API today. For those of you who don't know, formspring.me is the popular social networking site that allows people to ask and answer anonymous questions. It uses Facebook Connect for user auth, as well as letting users sign up on the site if they don't have a Facebook account.

Apparently, as a joke some time ago, I signed up for the beta API. So I decided to develop a Perl module allowing easy use of the Formspring.me API. You can follow the development of it on my <a href="http://github.com/worr/WWW--Formspring">github</a>. I plan to release this code to CPAN when I'm done so that other people can use it.

Since I'm new to the CPAN world, if one of you astute readers notices something blatantly wrong about my code, I'd appreciate it if you'd point it out.
