+++
title = "Formspring API Issues"
+++
I've been working on my Perl library for Formspring.me, and I've run into a pretty egregious issue. Apparently they aren't following the OAuth spec correctly, and aren't returning  a oauth_callback_confirmed parameter. The problem for me lies in that the wonderful Perl OAuth library, Net::OAuth requires that the OAuth response return this parameter, and errors if it's not present. I've emailed api@formspring.me about it, but they have yet to answer my email.

[1] http://tools.ietf.org/html/rfc5849#section-2.1
