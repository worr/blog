+++
title = "Update: spamd on Linux"
+++

I finally took some time this weekend to deploy spamd on some ec2 instances. I
have 2 mail servers on ubuntu 13.04 running the latest copy of spamd on linux,
with synchronized greylisting tables.

I've run into a few bugs with deployment, which I've added to the github
issues. Hopefully I can get them resolved soon.

I did run spamd through a small gamut of tests with postal, a SMTP stress
tester. They handled the excess load beautifully, keeping the synchronization
tables up to date even at 1000 mails/min (well, it was less than that, due to
spamd's resource draining feature) for 8 hours.

If you want to help test spamd, it'd be super great if you can sign up
<a href="mailto:ubuntu@spamd.worrbase.com">ubuntu@spamd.worrbase.com</a>
 for all kinds of sketchy services. Maybe even forge
the email address and reply to some spam messages (that's what I'm doing). I'd
like to continue testing it for the next week or so with some real-world spam
load.

The only bit left to port is spamlogd. I haven't figured out the most logical
way to translate it to iptables, since iptables lacks a logging device akin to
pflogd.

Again, please direct spam to <a href="mailto:ubuntu@spamd.worrbase.com">ubuntu@spamd.worrbase.com</a>
(or even <a href="mailto:hostmaster@spamd.worrbase.com">hostmaster@spamd.worrbase.com</a> to test greytrapping).
