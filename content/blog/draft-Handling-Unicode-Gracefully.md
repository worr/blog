+++
title = "Handling Unicode Gracefully"
draft = true
+++

Outline:

- introduction to unicode (explicitly scope to utf-8) (mention utf-ebcdic for
  laffs)
- talk about assumptions about character length (c)
- talk about wide characters in c
- demonstrate issues
    - combining characters
    - ZWJ hax
    - conv issues
- normalization???
    - brief discussion of types
    - demonstrate issues

other languages
- python
    - 2 vs 3
    - 3 conv weirdness
- golang
    - wtf len is broked
