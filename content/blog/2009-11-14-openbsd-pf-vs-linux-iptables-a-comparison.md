+++
title = "OpenBSD pf vs Linux iptables: A Comparison"
+++
This weekend, I decided it would be a good idea to turn my Linux router/firewall into an OpenBSD router/firewall. <a href="http://www.csh.rit.edu/~devnull/blog/">Clockfort</a> recommended it, so I decided to grab backups of my iptables rules, install OpenBSD, and learn pf.

I learned iptables about a year ago, when I first built this router/firewall. It acts like those little home gateways that you get; it does NAT, DHCP, DNS, etc. I've also used iptables to firewall my desktop and laptop (though those rule sets were significantly simpler than the firewall).
<h3>Configuration</h3>
The firstÂ notableÂ difference between pf and iptables: pf has a config file! It also has variables, lists and tables that you can manually populate which ease configuration. You can even include other config files in case you need to split your config for whatever reason. When you're done modifying the config file, just call pfctl -f /etc/pf.conf and it'll load that rule set and start filtering.

iptables doesn't have any of that. iptables are primarily populated through the iptables command. You can use iptables-save and iptables-restore to save and load iptables rules from a file. The file is basically a bunch of iptables commands with the iptables bit omitted. Another alternative is to write a bash script that loads your iptables rules one by one. That option gives you the benefit of using variables so that it's trivial to change IPs or similar.

I used iptables-save and restore to configure my iptables rules, and just wrote rules to that file in a similar syntax when I wanted to reconfigure parts of my firewall.
<h3>Rule Processing</h3>
The next most obvious way they differ is how they process rules. iptables has various tables, each with different chains that packets traverse, whereas pf just processes packets straight down the config file.

With pf, packets traverse the flat pf.conf file. Even if a packet matches a rule, it continues to process the packet all the way down the configuration file. Only if a rule contains the "quick" option does pf stop processing and take action before hitting the end of the rule set. If a packet makes it all the way to the end of the config file, the last action specified from a rule that matched that packet is taken.

With iptables, packets are processed by various chains in different order, depending on the source and destination in the packet. For example, normal outgoing packets are processed by the OUPUT chain on the filter table. Various rules within that chain may cause processing to hop over to a different set of rules on a user-defined chain, or might take action on a packet. When a packet matches a rule description, processing on that chain stops immediately, and the action is taken.
<h3>Dynamic Modification</h3>
pf really just owns iptables here. To dynamically update your rules with iptables, you just write new rules on the fly. If you want to do a bunch, you would write a bash script to delete some rules, and write new ones.

pf, you can change tables, variables, lists and anchors on the fly. Anchors are a really cool feature of pf. They are basically sub-rulesets that have names. So if you define an anchor somewhere in your ruleset, you can call pfctl and totally redefine the rules within that anchor. You can even write anchors to files, and load them from different files.
<h3>Packet Filtering</h3>
pf is pretty simple concerning what it can filter for. It can filter based on protocol, TCP flags, source IP, destination IP, interface, and port. There is also some slightly more advanced filtering, like antispoof, unicast reversing, and passive operating system finger printing. For the majority of situations, this kind of firewall control is fine.

iptables can do all of the same stuff. However with iptables, you can load all sorts of modules that do far more intensive filtering than pf. You can filter based on state (no, you can'tÂ filterÂ based on state in pf), where they are (geoip), time, statistics, ToS, and much more. There are so many target extensions for iptables, it is ridiculous. And if that isn't enough, you can pass the packet into userspace and write a script to filter it further there.
<h3>Performance</h3>
pf is fast. I said before that you can't filter packets in pf based on state. That doesn't mean that pf isn't a stateful firewall. It definitely recognizes state, as it passes packets that are part of an established connection without even processing them with pf. This means the majority of your packets skip your firewall rules entirely. This isn't nearly as insecure as it sounds, since most iptables rulesets pass packets that are part of an established state anyway. It definitely has the distinct advantage of making it faster though.

With iptables, all of your packets pass through all of your rules. This can really slow things down, especially if you have complicated rulesets. If you use all sorts of crazy iptables modules, that will slow it down pretty heavily too. And if you pass the packet into userspace for further processing, it will slow it down even more.
<h3>Conclusion</h3>
pf and iptables are both great firewalling solutions, but cater to people of different needs. pf is ridiculously fast, but lacks some of the more avanced features of iptables. Since my router/firewall box doesn't really need those advanced iptables features, and since it needs to be fast, I'm gonna stick with pf for now.
