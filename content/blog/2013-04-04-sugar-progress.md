+++
title = "Sugar Progress"
+++

Sugar progress has been slow so far - Matt and I have mostly been working on administrative stuff.

I specifically set up the project page, and communicated with the sugar community about the validity of some of our bugs.

Not much more to report than that.
