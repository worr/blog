+++
title = "Google Wave"
+++
Yeah, that's right. Just got an account. Well, by just, I mean I got one on Friday. Don't even ask for invites, because they were gone by Saturday morning. Also, if you don't know what Wave is, you should really go to <a href="http://wave.google.com">this</a> site and watch the video. I know it's long, but it's well worth it.

So Google Wave is pretty awesome. It's ridiculously buggy, but it works well enough if you aren't using it in a production environment. I have yet to try out any of the multimedia stuff beyond embedding YouTube videos in Wave, but I imagine it works just as well as everything else does. I'll probably talk more about that when I've played with it.

What I've really been playing with is the Wave API, in Python of course. The jist of what I've been reading is that the Java API isn't as up to par as the Python API. Also, I hate Java, and I hate writing code in Java.

The Wave API is built off of <a href="http://appspot.com">Google App Engine</a>, and has all the same limitations as App Engine. My roommate and I kinda half started working on an NNTP gateway to Wave. Unfortunately, the limitations App Engine sets makes this a little more difficult than we'd like (no sockets or anything of the sort).

The one thing I've come across in the API that I don't like, is that you *must* include the entire Python Wave module in the app directory that you upload to App Engine. Not a huge issue, but definitely a bit of an annoyance. The only real problems I see are the possibility for people to modify the version of the Wave API they upload, and that the version of the Wave API they upload is outdated.

Another issue with using App Engine as the platform for all of the Wave code and such, is that you *must* use Python 2.5. App Engine doesn't work with anything higher than that. So even though the Wave development site says you can use Python 2.5 or above, the above part is really a lie.

The good news: Google Wave won't forever be reliant on App Engine. Sometime in the future, they will be allowing Apps from anywhere. This is will be awesome, and I can't wait.

Oh, and I had other productive things I was gonna do this weekend, but they all got shot to hell when I got my Wave account.

Now you can wave me at ay1244@googlewave.com
