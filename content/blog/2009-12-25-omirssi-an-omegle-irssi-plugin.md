+++
title = "omirssi: An Omegle irssi plugin"
+++
Sometimes I really hate going home. I'm on Christmas break, and I've spent the majority of my time just watching movies in my basement. I got really bored, so yesterday morning while I was watching "Juno" (love that movie), I wrote an <a href="http://omegle.com" target="_self">Omegle</a> script for irssi. I just made up a <a href="http://code.google.com/p/omirssi" target="_self">google code</a> page for download, so have at it irssi users. Maybe I'll get around to adding it to the big list of irssi scripts sometime soon.
