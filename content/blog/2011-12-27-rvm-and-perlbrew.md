+++
title = "rvm and perlbrew"
+++

I was kind of an idiot in my last post. I wrote a ruby script to 
symlink my gems' executables to /usr/local/bin. Well that's dumb.  There's a
much better way to accomplish the same goal, with some healthier side effects!
It's called [rvm](http://beginrescueend.com/rvm/install/) (or Ruby Version 
Manager)!

rvm allows you to manage multiple versions of ruby that are installed to a
location in your home directory. You can select a version of ruby to use for
current session with 
  rvm use 1.9.2
That command will use ruby-1.9.2, provided you installed it with rvm. To
install:
  rvm install 1.9.2
When you have a version of ruby selected, gems will install into the
directory that houses that version of ruby, and won't pollute your
system install of ruby.

App::perlbrew (installable through cpan) does exactly the same thing, albeit
with Perl! I heartily recommend both if you do development with either ruby
or Perl.
