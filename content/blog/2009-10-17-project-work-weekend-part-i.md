+++
title = "Project Work Weekend Part I"
+++
So CSH is holding its first "Project Work Weekend" of the year, and so far, it's kinda been productive. Personally, I got a lot of work done. I've been kinda productive. I worked a little bit on my Omegle application that I've been playing with in my head, and I definitely got some of my CS project done.

For my CS project, me and a partner have to write a game playing framework, and then a bunch of pluggable games for the framework. It's a lot easier than it sounds, as they're all little command line games like take away, kayles, or connect 3. It's still pretty cool though. It's weird to be working with objects again after mostly programming in C this summer. But yeah, so the project is coming along. It's nice, because right now I only really have to write the abstract framework while my partner writes the game parts that inherit from my framework. Also I have to fix the old game. But whatever, that should be easy.

My Omegle app is coming along slowly. Trying to figure out perl GTK+ while figuring out perl OO while figuring out the ill-documented WWW::Omegle perl module. Fun times. Actually, typing this made me realize I should OO it up a little more. Gonna go back to work after this post.

I definitely need to finish up the slides for the presentations I'm gonna give CSH. I'm gonna publish them on here too, under a new "Presentations" section. It should look pretty hot.

Anyway, I should go back to being productive, cause I haven't done as much as I wanted.
