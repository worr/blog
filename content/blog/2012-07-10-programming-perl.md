+++
title = "Book Review: Programming Perl"
+++

**NOTE:**
*Hey! This is a book review I wrote some time ago that was published [here](http://www.csh.rit.edu/reviews?page=1). This review is outdated, mostly because there IS a new edition about the last major release of perl, perl 5.14. Because of the release of the new edition, I thought it might be good to reproduce the review on my blog.*

The camel book is a book that needs no introduction. The Perl community considers it the ultimate Perl book, and rightfully so, as it is written by Larry Wall himself (also Christiansen and Jon Orwant)!

The camel book is an in-depth look at the Perl programming language. The book does a fantastic job explaining how to use the language, the more powerful aspects of the language, as well as some of the implementation details of the compiler. As you progress through the book, it constantly builds upon itself. Different quirks/features of the language which are first introduced as black magic are explained in exhaustive detail later on. For example, you first learn about what the autoload pragma is, followed by advanced usage of goto, and then they tie that together by detailing how autoload is implemented with gotos. As a side note, I know this sounds gross, but gotos aren't what you think they are in Perl... at least not always.

The writing is, for the most part, very clear and understandable, and contains a heavy dose of humor. The humor is definitely welcome, as the text would be horribly dry without it. The humor also tends to be representative of the Perl community at large, which is fabulous for the new Perl programmer. The only problem I had with the writing style was the chapter on regular expressions. In this chapter, the wording is murky at times, and more examples would be a welcome addition. Like I said though, this is only one chapter.

The biggest problem with this book is that it's old. The latest edition (3rd at the time of this writing) only covers up to Perl 5.6. We're at Perl 5.12 now, with 5.14 on the horizon, and a significant amount has changed. However, that doesn't make this book irrelevant. I think it makes a wonderful companion to Modern Perl by chromatic. Once someone has a handle on how to write modern Perl (>=5.10), this book is definitely a great look at the rest of Perl as well as an interesting peek into the internals of the language. Regardless of recent developments in the language, this book still has a significant amount of relevant content.

If you want to truly become a Perl master, you need a copy of this book, but keep in mind you'll also probably want a book that covers more modern Perl.
