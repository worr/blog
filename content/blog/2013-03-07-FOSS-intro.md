+++
title = "FOSS Class Introduction"
+++

So, first, I royally messed this up when I first committed this blog entry, and
wrote about 3 things I enjoyed doing, rather than writing about 3 Sugar
activities. That post has been removed - largely out of embarrassment

That being said, here are my three favorite Sugar activities.

## Wine

The first activity I find interesting is Wine. This isn't because I think the
Wine project is particularly interesting by itself, but because I find it
surprising that Wine has been "ported" to the OLPC as well as its reception.

One reviewer basically says that it's how he does normal computing on his OLPC,
which is curious to me, as there really ought to be Activities to handle his
needs (in this case, playing games and listening to music).

## Etoys

Etoys was pretty much guaranteed to make this list, as I'm (obviously) a
programmer. It seems like a smart way to make programming more appealing to
children, so that they can get hooked at a young age.

## GetBooks

GetBooks looks great, as it makes book acquisition trivial for people who want
to read. I'm not sure what kinds of books you can find using OPDS, but the idea
by itself is laudable. I honestly can't wait to see what kinds of texts are
available to me.
