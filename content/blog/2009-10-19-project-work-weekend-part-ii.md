+++
title = "Project Work Weekend Part II"
+++
Yesterday ended up being extraordinarily productive for me. I learned GTK+ in perl, and wrote the GUI for my Omegle app. It's looking decent, although there is this annoying issue with the menu bar that's bothering me a bit. It's very simple, but I can't really think of any other features to put in the GUI.

I also got a bunch of the abstraction for my CS project done today. But that wasn't that much code. Oh well.

Also, in the middle of this post, got roped into helping fix our Xen management interface. And by help, I meant I installed Ubuntu server (ugh) on a machine and installed openssh. Woo.
