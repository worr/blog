+++
title = "My first COBOL program on z/OS"
+++
Today, I got Hello World working in COBOL on an IBM Z9. This is actually surprisingly more difficult than it would seem. But I'm gonna document it here so I remember how to do it, and also in case some random z/OS n00b stumbles upon this entry.

Here is the Hello World file I wrote:

```cobol
IDENTIFICATION DIVISION.
PROGRAM-ID. HELLO.
PROCEDURE DIVISION.
    DISPLAY 'Hello, World'.
    STOP RUN.
```

Seems pretty easy, right? Well, yeah, no shit. It's Hello World. I grabbed it from a Wikipedia article. It's not exactly innovative genius. However, compiling it and linking it is a different story.

I tried at first using the ISPF foreground COBOL compiler. However, this was very non-obvious, and the documentation was lacking. All the documentation I found consisted mostly of how to do it through TSO, where it made mention that an ISPF interface did exist.

So then I went the JCL route.

IBM has some great documentation on how to do this. However, it is definitely not in the 900+ page Enterprise COBOL for z/OS Programming Guide. That was really just confusing. <a href="http://publib.boulder.ibm.com/infocenter/zos/basics/index.jsp?topic=/com.ibm.zos.zappldev/toc.htm">Here is the great documentation.</a> This is what got me off the ground.

My JCL ended up looking like this:

```jcl
//CLGP JOB
//CLG EXEC IGYWCLG
//COBOL.SYSIN DD DSN=HLQ.EXPERIME.COBOL(HELLO),DISP=SHR
```

It's really pretty simple. The first line just declares the name of the job (CLGP), the next line executes the terribly named COBOL compile, link-editor, and run JCL procedure. IBM wrote it so you don't have to. The last line just loads the dataset member that contains all of the COBOL code.

See? Mainframes and COBOL are THAT easy!
