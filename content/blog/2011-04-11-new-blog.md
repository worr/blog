+++
title = "New blog"
+++
I've reworked my blog quite a bit. I transferred my some of my old
Wordpress posts to a new blogging engine -- jekyll.

Jekyll is pretty fantastic, in that it generates static sites from
posts done in markdown, and templates made with the Liquid templating
engine. It's gaining popularity thanks to being the technology that runs
github pages.

I heard about it from [Ryan Clough](http://becomingaweso.me/) a few years
ago (before it was cool), and then my friend 
[Russel Harmon](http://russell.harmon.name/) encouraged me to start using
it for my own blog. Since I accidentally took down my wordpress site
sometime ago, I decided to give it a go. I also decided to roll out nginx
instead of Apache since my servers are too old to really handle Apache 
effectively.

So far, I definitely like it. I love writing my blog in vim (the best
text editor) and doing version control with git. I still need to set up 
git hooks to auto-publish when I commit.

I write all of the entries on my OpenBSD dev server, charmeleon, and then
push the generated HTML to my web server, so I don't need to bother setting
up the jekyll tools on my web server or anything like that.

nginx is fast. It's really fast, and it's trivial to configure (for static
sites, anyway). I'm going to keep using it, and possibly try and use it
when I set up my personal Diaspora node.
