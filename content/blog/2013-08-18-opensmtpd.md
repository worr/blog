+++
title = "OpenSMTPD"
+++

I love [OpenSMTPD](http://opensmtpd.org/). After years of running a postfix 
installation, OpenSMTPD is such a joy to configure and use. The configuration
is dead simple, and it's pretty damn stable. I wrote a
[protip](https://coderwall.com/p/eejzja) about it over on Coderwall. I plan on
following up with more protips about more advanced configurations, with lmtp,
SSL/TLS and greylisting.

Obviously, it's now the mail server running mail on
[worrbase.com](http://worrbase.com), as well as the mail servers for my spamd
test cluster (remember to email
[ubuntu@spamd.worrbase.com](mailto:ubuntu@spamd.worrbase.com)!).

Oh, and my friend [Michail Yasonik](http://yasonik.com/) redesigned my blog.
It looks significantly better than what I had up before.
