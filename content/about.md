+++
date = 1990-03-29
title = "About Me"
template = "about.html"
+++

<section class="h-card vcard">
<img src="/img/self.jpg" alt="Selfie Shot" class="smallImg selfie u-photo photo">
<p class="p-note note">Hey, I'm <span class="p-name fn"><span class="p-given-name given-name">William</span> <span class="p-family-name family-name">Orr</span></span> or <span class="p-nickname nickname">worr</span>. I'm currently a <span class="p-job-title job-title">Devops Engineer</span> at Adevinta. Currently, I work with Kubernetes and Golang in the cloud, like the rest of the dang ops world these days. I also have an interest in systems programming, and focus on OS/systems programming projects in my limited spare time. Currently, I'm located in <span class="p-adr h-adr adr"><span class="p-locality locality">Barcelona</span>, <span class="p-country-name country-name">Spain</span></span>.</p>

<p> I'm a staunch <a href="https://openbsd.org">OpenBSD</a> user at home, with almost all of my systems running OpenBSD where possible (I have one system that has to run FreeBSD). Currently, I'm spending time getting more proficient in Go and Rust, and trying to make the experience of both on OpenBSD more palatable where I can.</p>

<h2>Contact Me</h2>
<dl>
    <span class="group">
        <dt>PGP Key:<dt>
        <dd class="contactItem"><a class="modalLink u-key key" href="https://pgp.worr.pub">95D8 225C 2EFE 2DA8 954F 41A1 E588 5A8A 4D91 B964</a></dd>
    </span>
    <span class="group">
        <dt>Gitlab:</dt>
        <dd class="contactItem"><a class="modalLink u-url url" href="https://gitlab.com/worr">gitlab.com/u/worr</a></dd>
    </span>
    <span class="group">
        <dt>Github:</dt>
        <dd class="contactItem"><a class="modalLink u-url url" href="https://github.com/worr">github.com/worr</a></dd>
    </span>
    <span class="group">
        <dt>LinkedIn:</dt>
        <dd class="contactItem"><a class="modalLink" href="http://www.linkedin.com/in/willorr">linkedin.com/in/willorr</a></dd>
    </span>
    <span class="group">
        <dt>Email:</dt>
        <dd class="contactItem"><a class="modalLink u-email email" href="mailto:will@worrbase.com">will@worrbase.com</a></dd>
    </span>
    <span class="group">
        <dt>Fediverse:</dt>
        <dd class="contactItem"><a class="modalLink u-url url" href="https://bsd.network/@worr">bsd.network/@worr</a></dd>
    </span>
    <span class="group">
        <dt>Twitter:</dt>
        <dd class="contactItem"><a class="modalLink" href="https://twitter.com/worr">twitter.com/worr</a></dd>
    </span>
    <span class="group">
        <dt>Openhub:</dt>
        <dd class="contactItem"><a class="modalLink" href="https://www.openhub.net/accounts/worr">openhub.net/accounts/worr</a></dd>
    </span>
</dl>
<a hidden class="u-url url" href="https://onwednesdayswewear.pink">onwednesdayswewear.pink</a>
<a hidden class="u-url url" href="https://worrbase.com">worrbase.com</a>
</section>
