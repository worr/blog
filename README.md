# worrbase.com

This is my blog

## Dependencies

All we require is zola

```sh
pkg_add -u zola
```

### Run-time deps

We used to have a dep on Google fonts, but those have been brought local via
[Google Webfonts Helper](https://google-webfonts-helper.herokuapp.com/fonts)

## Building

```sh
$ make
$ make serve  # this starts up a web server
```

## Deploying

```sh
$ make deploy
```
