ZOLA := /usr/local/bin/zola

.if exists(/usr/local/bin/rsync)
RSYNC := /usr/local/bin/rsync
.else
RSYNC := /usr/bin/openrsync
.endif

RSYNCFLAGS := --rsync-path=/usr/bin/openrsync

.PHONY := test build deploy clean

test:
	$(ZOLA) serve --drafts

build:
	$(ZOLA) build

deploy: build
	git push origin master
	$(RSYNC) $(RSYNCFLAGS) -av --delete public/ worr@kefka.worrbase.com:site/
	ssh -t worr@kefka.worrbase.com doas rsync -av site/ /var/www/htdocs/

clean:
	-rm -rf public
